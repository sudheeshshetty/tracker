var app = angular.module('myApp', ['ui.router','ionic','ngCordova']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('main',{
        url:'/main',
        views:{
            'header':{
                templateUrl: 'map.html',
                controller: 'HeaderController'
            }
        }
    })
    .state('home',{
        url:'/',
        views:{
            'header':{
                templateUrl:'main.html',
                controller:'MainController'
            }
        }
    })
}]);

app.controller('HeaderController',['$scope','$state','$cordovaGeolocation','$http',function($scope,$state,$cordovaGeolocation,$http){
    var mapOptions;
    var map;
    var onError=function(err){
      $scope.calldialog();
    }


    $scope.search=function(){
        alert("hi");
    }
    
  var onSuccess=function(position){


        $scope.lat  = position.coords.latitude;
        $scope.long = position.coords.longitude;
        
        //  $scope.map=map;
        var mylatlong = new google.maps.LatLng($scope.lat,$scope.long);
        mapOptions={

          center: mylatlong,
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP

        };
        map = new google.maps.Map(document.getElementById("map"),mapOptions);
        new google.maps.Marker({
              position: mylatlong,
              map: map,
              title: "Your Location"
              
          });
      
     var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'latLng': mylatlong }, function (results, status) {
        var result = results[0];
        var state = '';

        for (var i = 0, len = result.address_components.length; i < len; i++) {
            var ac = result.address_components[i];

            if (ac.types.indexOf('administrative_area_level_2') >= 0) {
                city = JSON.stringify(ac);
            }
        }

        alert(city);

    });
  }

  $scope.calldialog= function() {
      document.addEventListener("deviceready",function() {
      cordova.dialogGPS("Your GPS is Disabled, this app needs to be enable to works.",//message
                        "Use GPS, with wifi or 3G.",//description
                        function(buttonIndex){//callback
                        switch(buttonIndex) {
                            case 0: break;//cancel
                            case 1: break;//neutro option
                            case 2: var posOptions = {timeout: 5000, enableHighAccuracy: false};
                                    var pos=$cordovaGeolocation.getCurrentPosition(posOptions);
                                    pos.then(onSuccess, onError);//user go to configuration
                        }},
                        "Please Turn on GPS",//title
                        ["Cancel","Later","Go"]);//buttons
                      });
  }
  var posOptions = {timeout: 1000, enableHighAccuracy: false};
   var pos=$cordovaGeolocation.getCurrentPosition(posOptions);
   pos.then(onSuccess, onError);
}]);


app.controller('MainController',['$scope','$state',function($scope,$state){
    $scope.switchState = function(){
        $state.go('main');
    }
}]);